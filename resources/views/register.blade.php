@extends('layout.master')
@section('titlet')
<h1>Buat Account Baru!</h1>
@endsection
@section('sub-title')
<h3>Sign Up Form</h3>
@endsection
@section('content')
     <!-- Form -->
     <form action="/save" method="post">
          @csrf
          <label for="firstname">First name: </label> <br> <br>
          <input type="text" name="firstname" id="firstname"> <br> <br>
          <label for="lastname">Last name:</label> <br> <br>
          <input type="text" name="lastname" id="lastname"> <br> <br>
          <!-- Radio-Button -->
          <label>Gender: </label> <br> <br>
          <input type="radio" name="gender" id="male">
          <label for="male" value="Male">Male</label><br><br>
          <input type="radio" name="gender" id="female">
          <label for="female">Female</label><br><br>
          <input type="radio" name="gender" id="other">
          <label for="other">Other</label><br><br>
          <!-- End Radio-Button -->
          <!-- Drop-down List  -->
          <label for="nationality">Nationality: </label><br><br>
          <select name="nationality" id="nationality">
               <option value="Indonesian">Indonesian</option>
               <option value="USA">USA</option>
               <option value="Singapore">Singapore</option>
          </select><br><br>
          <!-- End Drop-down List -->
          <!-- Checkbox  -->
          <label>Language Spoken: </label><br><br>
          <input type="checkbox" name="indonesia" id="indonesia">
          <label for="indonesia">Indonesia</label><br>
          <input type="checkbox" name="english" id="english">
          <label for="english">English</label><br>
          <input type="checkbox" name="other2" id="other2">
          <label for="other2">Other</label><br><br>
          <!-- end Checkbox -->
          <!-- Text Area -->
          <label for="bio">Bio</label><br><br>
          <textarea name="bio" id="bio" cols="20" rows="10"></textarea><br><br>
          <!-- End Text Area -->
          <!-- button -->
          <input type="submit" value="Sign Up">
          <!-- end Button -->
     </form>
@endsection
