<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>AdminLTE 3</title>

     <!-- Google Font: Source Sans Pro -->
     <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
     <!-- Font Awesome -->
     <link rel="stylesheet" href={{('/template/plugins/fontawesome-free/css/all.min.css')}}>
     <!-- Theme style -->
     <link rel="stylesheet" href={{('/template/plugins/fontawesome-free/css/all.min.css')}}>
     <!-- Ionicons -->
     <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
     <!-- Tempusdominus Bootstrap 4 -->
     <link rel="stylesheet" href={{('/template/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}>
     <!-- iCheck -->
     <link rel="stylesheet" href={{('/template/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}>
     <!-- JQVMap -->
     <link rel="stylesheet" href={{('/template/plugins/jqvmap/jqvmap.min.css')}}>
     <!-- Theme style -->
     <link rel="stylesheet" href={{('/template/dist/css/adminlte.min.css')}}>
     <!-- overlayScrollbars -->
     <link rel="stylesheet" href={{('/template/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}>
     <!-- Daterange picker -->
     <link rel="stylesheet" href={{('/template/plugins/daterangepicker/daterangepicker.css')}}>
     <!-- summernote -->
     <link rel="stylesheet" href={{('/template/plugins/summernote/summernote-bs4.min.css')}}>
@stack('styles')
</head>

<body class="hold-transition sidebar-mini">
     <!-- Site wrapper -->
     <div class="wrapper">
          <!-- Navbar -->
          @include('partial.navbar');
          <!-- /.navbar -->

          <!-- Main Sidebar Container -->
          <aside class="main-sidebar sidebar-dark-primary elevation-4">
               @include('partial.sidebar');
          </aside>
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
               <!-- Content Header (Page header) -->
               <section class="content-header">
                    <div class="container-fluid">
                         <div class="row mb-2">
                              <div class="col-sm-6">
                              @yield('title')
                              </div>
                              <div class="col-sm-6">
                                   <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item active">Blank Page</li>
                                   </ol>
                              </div>
                         </div>
                    </div><!-- /.container-fluid -->
               </section>

               <!-- Main content -->
               <section class="content">

                    <!-- Default box -->
                    <div class="card">
                         <div class="card-header">
                              <h3 class="card-title">@yield('sub-title')</h3>
                              <div class="card-tools">
                                   <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                   </button>
                                   <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                        <i class="fas fa-times"></i>
                                   </button>
                              </div>
                         </div>
                         <div class="card-body">
                              @yield('content')
                         </div>
                         <!-- /.card-body -->
                         <div class="card-footer">
     
                         </div>
                         <!-- /.card-footer-->
                    </div>
                    <!-- /.card -->

               </section>
               <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->

          <footer class="main-footer">
               <div class="float-right d-none d-sm-block">
                    <b>Version</b> 3.2.0
               </div>
               <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
          </footer>

          <!-- Control Sidebar -->
          <aside class="control-sidebar control-sidebar-dark">
               <!-- Control sidebar content goes here -->
          </aside>
          <!-- /.control-sidebar -->
     </div>
     <!-- ./wrapper -->

     <!-- jQuery -->
     <script src={{('/template/plugins/jquery/jquery.min.js')}}></script>
     <!-- Bootstrap 4 -->
     <script src={{('/template/plugins/bootstrap/js/bootstrap.bundle.min.js')}}></script>
     <!-- AdminLTE App -->
     <script src={{("/template/dist/js/adminlte.min.js")}}></script>

     @stack('scripts')
</body>

</html>