<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Auth()
    {
        return view('register');
    }


    public function save(Request $request)
    {
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];

        return view('welcome', ['fn' => $firstname, 'ln' => $lastname]);
    }
}
